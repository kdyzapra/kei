import { combineReducers } from 'redux';

import itemsReducer from './items';
import showFilterReducer from './show-filter';

const combinedReducer = combineReducers({
    itemsReducer,
    showFilterReducer,
})

export default combinedReducer;
