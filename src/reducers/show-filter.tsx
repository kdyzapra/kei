import { UPDATE_SHOW_ACTIVE, UPDATE_SHOW_DONE } from '../actions/action-list';

const initialState = {
    showActive: true,
    showDone: true,
}

export default function reducer(state = initialState, action: any) {
    switch(action.type) {
        case UPDATE_SHOW_ACTIVE:
            return {
                ...state,
                showActive: action.payload
            }
        case UPDATE_SHOW_DONE:
            return {
                ...state,
                showDone: action.payload
            }
        default:
            return state;
        
    }
}