import { ADD_ITEM, UPDATE_ITEM, REMOVE_ITEM, TOGGLE_ITEM_IS_DONE } from '../actions/action-list'

const defaultItem = { value: '', isDone: false };
const initialState = {
    items: [{ ...defaultItem , id: 0, index: 0 }],
}

function getUpdatedItems(items: any, value: string, index: number, toggleIsDone: boolean = false) {
    const isDone = toggleIsDone ? !items[index].isDone : items[index].isDone
    const newItem = { ...items[index], value, isDone }
    return [
        ...items.slice(0, index ),
        newItem,
        ...items.slice(index + 1)
    ]
}

export default function reducer(state = initialState, action: any) {
    const { items } = state;
    console.log("test", action.type, items)
    switch(action.type) {
        case ADD_ITEM: {
            const { value, index } = action.payload.item
            return {
                ...state,
                items: [
                    ...getUpdatedItems(items, value, index),
                    {
                        ...defaultItem,
                        id: items[items.length - 1].id + 1,
                        index: items[items.length - 1].index + 1,
                    }
                ]
            }
        }
        case UPDATE_ITEM: {
            const { value, index } = action.payload.item
            return {
                ...state,
                items: getUpdatedItems(items, value, index)
            }
        }
        case REMOVE_ITEM: {
            const { index } = action.payload.item
            const reIndexedItems = items.slice(index + 1).map((elem, ind) => {
                return { ...elem, index: ind + index}
            })
            const newItems = [
                ...items.slice(0, index),
                ...reIndexedItems
            ]
            return {
                ...state,
                items: newItems
            }
        }
        case TOGGLE_ITEM_IS_DONE: {
            const { index } = action.payload.item
            return {
                ...state,
                items: getUpdatedItems(items, items[index].value, index, true)
            }
        }
        default:
            return state;
    }
}