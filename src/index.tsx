import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import combinedReducer from './reducers/index'

import List from './components/list/list'

const store = createStore(combinedReducer);

ReactDOM.render(
    <Provider store={store}>
        <List>
        </List>
    </Provider>,
    document.getElementById("root")
)

