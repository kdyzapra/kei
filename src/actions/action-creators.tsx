import * as actions from './action-list';

export const modifyItem = (value: string, index: number, isLast = false) => {
    const item = {
        value,
        index
    }
    if(!value) {
        return {
            type: actions.REMOVE_ITEM,
            payload: { item }
        }
    }
    if(isLast) {
        return {
            type: actions.ADD_ITEM,
            payload: { item }
        }
    }
    return {
        type: actions.UPDATE_ITEM,
        payload: { item }
    }
}

export const toggleItemIsDone = (index: number) => {
    return {
        type: actions.TOGGLE_ITEM_IS_DONE,
        payload: { item: { index } },
    }
}

export const setShowActiveFilter = (state: boolean) => {
    return {
        type: actions.UPDATE_SHOW_ACTIVE,
        payload: state,
    }
}

export const setShowDoneFilter = (state: boolean) => {
    return {
        type: actions.UPDATE_SHOW_DONE,
        payload: state,
    }
}