export const ADD_ITEM= 'ADD_ITEM';
export const UPDATE_ITEM= 'UPDATE_ITEM';
export const REMOVE_ITEM= 'REMOVE_ITEM';
export const TOGGLE_ITEM_IS_DONE= 'TOGGLE_ITEM_IS_DONE';

export const UPDATE_SHOW_ACTIVE= 'UPDATE_SHOW_ACTIVE';
export const UPDATE_SHOW_DONE= 'UPDATE_SHOW_DONE';
