import React from 'react';
import Item from '../item/item'
import { connect } from 'react-redux';
import { modifyItem, toggleItemIsDone, setShowActiveFilter, setShowDoneFilter } from '../../actions/action-creators'

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import './list.css'

function mapStateToProps({ itemsReducer, showFilterReducer }: any) {
    return {
        items: itemsReducer.items,
        showActive: showFilterReducer.showActive,
        showDone: showFilterReducer.showDone,
    }
}

function mapDispatchToProps(dispatch: any) {
    return({
        modifyItem: (value: string, index: number, islast: boolean = false) => dispatch(modifyItem(value, index, islast)),
        toggleItemIsDone: (index: number) => dispatch(toggleItemIsDone(index)),
        setShowFilters: (filterType: number) => {
            switch(filterType) {
                case 0: 
                    dispatch(setShowActiveFilter(true))
                    dispatch(setShowDoneFilter(true))
                    return;
                case 1: 
                    dispatch(setShowActiveFilter(true))
                    dispatch(setShowDoneFilter(false))
                    return;
                case 2: 
                    dispatch(setShowActiveFilter(false))
                    dispatch(setShowDoneFilter(true))
                    return;
                default:
                    dispatch(setShowActiveFilter(true))
                    dispatch(setShowDoneFilter(true))
                    return;
            }
        }
    })
}

interface listProps {
    items: any[],
    showActive: boolean,
    showDone: boolean,
    modifyItem(value: string, index: number, islast?: boolean): any,
    toggleItemIsDone(index: number): any,
    setShowFilters(filterType: number): any,
}

class List extends React.Component<listProps> {
    getFilterType(showActive: boolean, showDone: boolean) {
        if(showActive && showDone) {
            return 0
        }
        return showActive ? 1 : 2
    }

    render() {
        const items = this.props.items.filter((item) => {
            if(this.props.showActive && this.props.showDone) {
                return item
            }
            return this.props.showActive ? !item.isDone : item.isDone
        }).map((element, index) => {
            return(
                <Item 
                key={element.id} 
                value={element.value}
                isDone={element.isDone}
                onTextChange={(value: string) => this.props.modifyItem(value, element.index, element.index === this.props.items.length - 1)}
                onCheckboxToggle={() => this.props.toggleItemIsDone(element.index)}
                />
            )
        })
        return(
            <Paper>
                <div className="listTitle">TODO</div>
                <Tabs 
                value={this.getFilterType(this.props.showActive, this.props.showDone)}
                indicatorColor="secondary"
                textColor="secondary" 
                variant="fullWidth"
                onChange={(event, tab) => {this.props.setShowFilters(tab)}}>
                    <Tab label="All" />
                    <Tab label="Active" />
                    <Tab label="Done" />
                </Tabs>
                {items}
            </Paper>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)