import React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';

import './item.css'

function isDone(isDone: boolean) {
    return isDone ? 'done' : 'notDone';
}

export default function item(props: any) {
    return(
        <Card className={`item ${isDone(props.isDone)}`}>
            <Checkbox 
            disabled={!props.value} 
            checked={props.isDone} 
            onChange={() => props.onCheckboxToggle()}/>
            <TextField 
            value={props.value} 
            placeholder="New Item"
            className="itemTextField"
            InputProps={{
                disableUnderline: true,
                classes: { input: "itemInputTextField" }
             }}
            onChange={(event) => props.onTextChange(event.target.value)}/>
        </Card>
    )
}